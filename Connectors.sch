EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 60F83FDF
P 1700 1850
AR Path="/60F83FDF" Ref="J?"  Part="1" 
AR Path="/60F7AF7D/60F83FDF" Ref="J2"  Part="1" 
F 0 "J2" H 1808 2131 50  0000 C CNN
F 1 "I2C Connector" H 1808 2040 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1700 1850 50  0001 C CNN
F 3 "~" H 1700 1850 50  0001 C CNN
	1    1700 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 60F83FE5
P 1700 2650
AR Path="/60F83FE5" Ref="J?"  Part="1" 
AR Path="/60F7AF7D/60F83FE5" Ref="J3"  Part="1" 
F 0 "J3" H 1808 2931 50  0000 C CNN
F 1 "Serial Connector" H 1808 2840 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1700 2650 50  0001 C CNN
F 3 "~" H 1700 2650 50  0001 C CNN
	1    1700 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 60F83FEB
P 1700 5300
AR Path="/60F83FEB" Ref="J?"  Part="1" 
AR Path="/60F7AF7D/60F83FEB" Ref="J4"  Part="1" 
F 0 "J4" H 1750 5617 50  0000 C CNN
F 1 "ICSP Connector" H 1750 5526 50  0000 C CNN
F 2 "Connector_PinSocket_1.00mm:PinSocket_2x03_P1.00mm_Vertical_SMD" H 1700 5300 50  0001 C CNN
F 3 "~" H 1700 5300 50  0001 C CNN
	1    1700 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x09_Male J?
U 1 1 60F83FF1
P 1650 3700
AR Path="/60F83FF1" Ref="J?"  Part="1" 
AR Path="/60F7AF7D/60F83FF1" Ref="J1"  Part="1" 
F 0 "J1" H 1758 4281 50  0000 C CNN
F 1 "Digital Pins" H 1758 4190 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 1650 3700 50  0001 C CNN
F 3 "~" H 1650 3700 50  0001 C CNN
	1    1650 3700
	1    0    0    -1  
$EndComp
Text HLabel 2000 1750 2    50   Input ~ 0
GND
Text HLabel 2000 1850 2    50   Input ~ 0
Vcc
Text HLabel 2000 1950 2    50   Input ~ 0
SDA
Text HLabel 2000 2050 2    50   Input ~ 0
SCK
Text HLabel 2050 5200 2    50   Input ~ 0
Vcc
Text HLabel 2050 5300 2    50   Input ~ 0
MOSI
Text HLabel 2050 5400 2    50   Input ~ 0
GND
Text HLabel 1450 5200 0    50   Input ~ 0
MISO
Text HLabel 1450 5300 0    50   Input ~ 0
SCK
Text HLabel 1450 5400 0    50   Input ~ 0
RESET
Text HLabel 2000 2550 2    50   Input ~ 0
GND
Text HLabel 2000 2650 2    50   Input ~ 0
Vcc
Text HLabel 2000 2750 2    50   Input ~ 0
RX
Text HLabel 2000 2850 2    50   Input ~ 0
TX
Text HLabel 1950 3300 2    50   Input ~ 0
D2
Text HLabel 1950 3400 2    50   Input ~ 0
D3
Text HLabel 1950 3500 2    50   Input ~ 0
D4
Text HLabel 1950 3600 2    50   Input ~ 0
D5
Text HLabel 1950 3700 2    50   Input ~ 0
D6
Text HLabel 1950 3800 2    50   Input ~ 0
D7
Text HLabel 1950 3900 2    50   Input ~ 0
D8
Text HLabel 1950 4000 2    50   Input ~ 0
GND
Text HLabel 1950 4100 2    50   Input ~ 0
Vcc
Wire Wire Line
	1900 1750 2000 1750
Wire Wire Line
	1900 1850 2000 1850
Wire Wire Line
	1900 1950 2000 1950
Wire Wire Line
	1900 2050 2000 2050
Wire Wire Line
	1900 2550 2000 2550
Wire Wire Line
	1900 2650 2000 2650
Wire Wire Line
	1900 2750 2000 2750
Wire Wire Line
	1900 2850 2000 2850
Wire Wire Line
	1850 3300 1950 3300
Wire Wire Line
	1850 3400 1950 3400
Wire Wire Line
	1850 3500 1950 3500
Wire Wire Line
	1850 3600 1950 3600
Wire Wire Line
	1850 3700 1950 3700
Wire Wire Line
	1850 3800 1950 3800
Wire Wire Line
	1850 3900 1950 3900
Wire Wire Line
	1850 4000 1950 4000
Wire Wire Line
	1850 4100 1950 4100
Wire Wire Line
	2000 5200 2050 5200
Wire Wire Line
	2000 5300 2050 5300
Wire Wire Line
	2000 5400 2050 5400
Wire Wire Line
	1450 5200 1500 5200
Wire Wire Line
	1450 5300 1500 5300
Wire Wire Line
	1450 5400 1500 5400
$EndSCHEMATC
